<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    //
    protected $fillable = ['title',
        'description',
        'article',
        'rank',
        'image_name',
        'image_path',
        'article_date'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $table = 'articles';
}
