<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use Alert;
use Exception;
use Illuminate\Support\Facades\Storage;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::orderBy('article_date','desc')->paginate(15);
        return view('home.index')
         ->with('articles', $articles);
    }

    public function create(){
        // $current_time = Carbon::now();
        // $old_time = Carbon::createFromTimestamp(Cache::store('articleDate')->get('date'));
        // if($current_time->diffInWeeks($old_time) > 3){
        //   Cache::store('articleDate')->put($current_time->timestamp);  
        // }
        return view('home.create');
    }


    public function store(Request $request){
        //validate
        $validatedData = $this->validate(request(),[
            'article_date'     => 'bail|required|date',
            'rank'        => 'bail|required|numeric',
            'title'       => 'required',
            'article'     => 'required',
            'description' => 'bail|required|max:500',
            'image'       => 'bail|nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        try{
            $article = new Article($request->input());

            if($request->hasFile('image')){
                $file = $request->file('image');
//                Storage::putFile('public/thumbnails', $file);
                $request->file('image')->store('public/thumbnails');
                $article->image_path = asset(Storage::url('thumbnails/'.$file->hashName()));
                $article->image_name = $file->hashName();
            }
            $article->save();
            Alert::success('Article saved successfully', 'Success');
        }catch (Exception $exception){
            Alert::error('Article not saved. Please try again', 'Error');
        }
        return back();
    }

    public function show($id){
        $article = Article::find($id);
        return view('home.show')
            ->with('article', $article);
    }

    public function edit($id){
        $article = Article::find($id);
        return view('home.edit')
            ->with('article', $article);
    }

    public function update($id, Request $request){
//        print_r($_POST);
//        die();
        //validate
        $validatedData = $this->validate(request(),[
            'article_date'     => 'bail|required|date',
            'rank'        => 'bail|required|numeric',
            'title'       => 'required',
            'article'     => 'required',
            'description' => 'bail|required|max:500',
            'image'       => 'bail|nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        try{
            $article = Article::find($id);

            if($request->hasFile('image')){
                //delete old file
                //check if exists first
                if (isset($article->image_name) && !trim($article->image_name)===''){
                    Storage::delete('public/thumbnails/'.$article->image_name);
//                unlink($article->image_path);

                    //store new file
                    $file = $request->file('image');
                    $request->file('image')->store('public/thumbnails');
                    $article->image_path = asset(Storage::url('thumbnails/'.$file->hashName()));
                    $article->image_name = $file->hashName();
                }else{
                    $file = $request->file('image');
//                Storage::putFile('public/thumbnails', $file);
                    $request->file('image')->store('public/thumbnails');
                    $article->image_path = asset(Storage::url('thumbnails/'.$file->hashName()));
                    $article->image_name = $file->hashName();
                }
            }
            $article->title = $request->title;
            $article->article_date = $request->article_date;
            $article->rank = $request->rank;
            $article->article = $request->article;
            $article->description = $request->description;
            $article->save();
            Alert::success('Article edited successfully', 'Success');
        }catch (Exception $exception){
            Alert::error('Article not edited. Please try again', 'Error');
        }

        return redirect('home');
    }

    public function destroy($id)
    {
        try{
            $article = Article::find($id);
            if($this->isNotEmpty($article->image_name)){
                Storage::delete('public/thumbnails/'.$article->image_name);
            }
            $article->delete();
            \Session::flash('message', 'Successfully deleted the article!');
//            Alert::success('Article deleted successfully', 'Success');
        }catch (Exception $exception){
            Alert::error('Article not deleted. Please try again', 'Error');
        }

        return redirect('home');
    }

    function isNotEmpty($input){
        $strTemp = $input;
        $strTemp = trim($strTemp);

        if($strTemp !== '')
        {
            return true;
        }

        return false;
    }
}
