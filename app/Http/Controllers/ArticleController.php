<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use Illuminate\Support\Collection;
use Carbon\Carbon;

class ArticleController extends Controller
{
    public function index()
    {
        $dates = Article::select('article_date')->distinct()->get();

        $feed = [];
        foreach($dates as $dute){
            $articles = Article::where('article_date', $dute->article_date)->get();
            $dt = Carbon::parse($dute->article_date);

            $feed[]=[
                'date' => $dt->timestamp,
                'articles' => $articles
            ];
        }
        // return Article::orderBy('article_date','desc')->get();
        return $feed;
    }
 
    public function show($date)
    {
        return Article::where('article_date',$date)->get();
    }

    public function order()
    {
        $dates = Article::select('article_date')->orderBy('article_date','desc')->distinct()->get();
        $duted = [];
        foreach($dates as $dt){
            $ts = Carbon::parse($dt->article_date);

            $duted[]=[
                'article_date' => $ts->timestamp
            ];
        }
        return $duted;
    }
}
