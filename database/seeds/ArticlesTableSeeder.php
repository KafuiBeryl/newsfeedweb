<?php

use Illuminate\Database\Seeder;
use App\Article;

class ArticlesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Article::truncate();

        $faker = \Faker\Factory::create();

        // And now, let's create a few articles in our database:
        for ($i = 0; $i < 50; $i++) {
            Article::create([
                'title' => $faker->catchPhrase,
                'description' => $faker->sentence($nbWords = 15, $variableNbWords = true),
                'article' => $faker->paragraph($nbSentences = 20, $variableNbSentences = true),
                'image_path' => 'https://pixabay.com/get/ef30b8092df51c22d9584518a33219c8b66ae3d018b7124794f8c77a/head-659651_1920.png?attachment',
                'article_date' => $faker->date($format = 'Y-m-d', $max = 'now'),
            ]);
        }
    }
}
