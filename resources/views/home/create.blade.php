@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <a class="btn btn-info" style="margin-bottom: .5em;" href="{{route('home.index')}}">Return to home</a>
            <div class="panel panel-info">
                <div class="panel-heading">Please fill out the form below to add a new article</div>

                <div class="panel-body">

                    @if (\Session::has('success'))
                        <div class="alert alert-success">
                            <p>{{ \Session::get('success') }}</p>
                        </div><br />
                    @endif

                    <form class="form" role="form" enctype="multipart/form-data" method="post" action="{{route('home.store')}}" >
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('article_date') ? ' has-error' : '' }}">
                            <label for="article_date" class="pushDown col-sm-4 control-label">Article Date</label>

                            <div class="pushDown col-md-8">
                                <input id="article_date" type="date" class="form-control" name="article_date" value="{{ old('article_date') }}" required autofocus>

                                @if ($errors->has('article_date'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('article_date') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('rank') ? ' has-error' : '' }}">
                            <label for="rank" class="pushDown col-sm-4 control-label">Article Number</label>

                            <div class="pushDown col-md-8">
                                <input id="rank" type="number" min="1" class="form-control" name="rank" value="{{ old('rank') }}" required autofocus>

                                @if ($errors->has('rank'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('rank') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <label for="title" class="pushDown col-sm-4 control-label">Title</label>

                            <div class="pushDown col-md-8">
                                <input id="title" type="text" class="form-control" name="title" value="{{ old('title') }}" required autofocus>

                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label for="description" class="pushDown col-sm-4 control-label">Description (30 words min)</label>

                            <div class="pushDown col-md-8">
                                <input id="description" type="text" class="form-control" name="description" value="{{ old('description') }}" required>

                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                            <label for="image" class="pushDown col-sm-4 control-label">Thumbnail Image</label>

                            <div class="pushDown col-md-8">
                                <label class="custom-file">
                                    <input id="image" type="file" value="{{ old('image') }}" class="custom-file-input form-control-file" name="image" accept="image/*">
                                    <span class="custom-file-control"></span>
                                </label>


                                @if ($errors->has('image'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('article') ? ' has-error' : '' }}">
                            <label for="article" class="pushDown col-md-4 control-label">Article</label>

                            <div class="pushDown col-md-8">
                                <textarea id="article" class="form-control tinyMad" rows="20" name="article">{{ old('article') }}</textarea>

                                @if ($errors->has('article'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('article') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Submit the article
                                </button>

                                {{--  <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Forgot Your Password?
                                </a>  --}}
                            </div>
                        </div> 
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
