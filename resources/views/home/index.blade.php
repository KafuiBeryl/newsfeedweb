@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-2">
            <div class="panel panel-default">
                {{--  <div class="panel-heading">What would you like to do today?</div>  --}}

                <div class="panel-body">
                    <div class="clearfix"></div>
                    <div class="left">
                        <a class="btn btn-primary" href="{{route('home.create')}}">
                            Add a new newsletter
                        </a>
                    </div>

                    {{--  <div class="left">
                        <a class="btn btn-primary" href="{{route('editnews')}}">
                            Edit newsletters
                        </a>
                    </div>  --}}
                    <div class="clearfix"></div>

                    <!-- will be used to show any messages -->
                    @if (Session::has('message'))
                        <div class="alert alert-info">{{ Session::get('message') }}</div>
                    @endif

                    <div class="clearfix"></div>
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Article Date</th>
                                <th>Title</th>
                                <th>Description</th>
                                {{--<th>Article</th>--}}
                                {{--<th>Image Path</th>--}}

                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($articles as $key => $value)
                            <tr>
                                <td>{{\Carbon\Carbon::parse($value->article_date)->format('d-M-Y')}}</td>
                                <td>{{$value->title}}</td>
                                <td>{{str_limit($value->description,100)}}</td>
{{--                                <td>{{str_limit($value->article,20)}}</td>--}}
{{--                                <td>{{str_limit($value->image_path,10)}}</td>--}}

                                <!-- we will also add show, edit, and delete buttons -->
                                <td>


                                    <!-- show the nerd (uses the show method found at GET /nerds/{id} -->
                                    <a class="btn btn-small btn-success" href="{{route('home.show', ['id' => $value->id])}}" title="Show"><i class="fas fa-eye"></i></a>

                                    <!-- edit this nerd (uses the edit method found at GET /nerds/{id}/edit -->
                                    <a class="btn btn-small btn-info" href="{{route('home.edit', ['id' => $value->id])}}" title="Edit"><i class="fas fa-edit"></i></a>

                                    <!-- delete the nerd (uses the destroy method DESTROY /nerds/{id} -->
                                    <!-- we will add this later since its a little more complicated than the other two buttons -->
                                    <form class="form" role="form" method="POST" action="{{route('home.destroy', ['id' => $value->id])}}" >
                                        {{ csrf_field() }}
                                        <input type="hidden" name="_method" value="DELETE">
                                        <button type="submit" class="btn btn-small btn-danger" title="Delete"><i class="fas fa-trash-alt"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $articles->links() }}
                   
                </div>
                
            </div>
        </div>
    </div>
    
</div>
@endsection
