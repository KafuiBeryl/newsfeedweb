@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-2">
                <a class="btn btn-info" style="margin-bottom: .5em;" href="{{route('home.index')}}">Return to home</a>
                <div class="panel panel-default">
                    {{--  <div class="panel-heading">What would you like to do today?</div>  --}}

                    <div class="panel-body">
                        <h2>Showing article {{$article->rank}} added on {{\Carbon\Carbon::parse($article->article_date)->format('d-M-Y')}}</h2>
                        <div>
                           <h3>{{$article->title}}</h3>
                            <h4>{{$article->description}}</h4>
                            <div class="clearfix"></div>
                            <img class="thumbnail" style="max-height: 12em; max-width: 12em" src="{{$article->image_path}}"/>
                            <div class="clearfix"></div>
                            <h5>{!! $article->article !!}</h5>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
@endsection